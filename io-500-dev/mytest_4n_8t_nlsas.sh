#!/bin/bash

#SBATCH -p debug
#SBATCH -w spartan-gpgpu[001,013,024,035]
#SBATCH --ntasks=32
#SBATCH --tasks-per-node=8
#SBATCH --mem=100G
#SBATCH --cpus-per-task=2
#SBATCH --time=06:00:00

module load OpenMPI/3.1.3-GCC-6.2.0-ucx

./io500_4n_8t_nlsas.sh
